from flask import Flask, jsonify, request, Response, make_response
from functools import wraps
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from passlib.apps import custom_app_context as passwrod_context
from flask_cors import CORS
import logging

from utils import verify_otp, do_response
import os
import jwt
import datetime
import response_codes as r_code

# Init app
app = Flask(__name__)
CORS(app)
app.config['SECRET_KEY'] = 'thismustbeasecretkey'
basedir = os.path.abspath(os.path.dirname(__file__))

# Database
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://flask_usr:12345@localhost:5432/flask_db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)

# Marshmallow
ma = Marshmallow(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(100))
    lastname = db.Column(db.String(100))
    username = db.Column(db.String(100), unique=True)
    password_hash = db.Column(db.String(64))
    email = db.Column(db.String(255), unique=True, nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, firstname, lastname, username, password, email, registered_on, admin=False):
        self.firstname = firstname
        self.lastname = lastname
        self.username = username
        self.password_hash = password  # passwrod_context.encrypt(password)
        self.email = email
        self.registered_on = datetime.datetime.today()
        self.admin = admin


class UserSchema(ma.Schema):
    class Meta:
        fields = ['id', 'username', 'firstname', 'lastname', 'email']


# Init user schema
user_schema = UserSchema()
users_schema = UserSchema(many=True)

user_test_case = {'id': '0', 'firstname': "Pudge", 'lastname': 'Invokerovich', 'username': 'pudge',
                  'email': 'pudge@test.au', 'register_on': '2020-01-19 23:23:34', 'role': 'ork'}


class Transaction(db.Model):
    """ describe transaction Model """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    price = db.Column(db.Float)
    quantity = db.Column(db.Integer)
    pos = db.Column(db.String(100))
    currency = db.Column(db.String(100))
    country = db.Column(db.String(100))

    def __init__(self, name, price, quantity, pos, currency, country):
        self.name = name
        self.price = price
        self.quantity = quantity
        self.pos = pos
        self.currency = currency
        self.country = country


class TransactionSchema(ma.Schema):
    class Meta:
        fields = ['id', 'name', 'price', 'quantity', 'pos', 'currency', 'country']


# Init transaction schema
transaction_schema = TransactionSchema()
transactions_schema = TransactionSchema(many=True)

transaction_test_case = {'id': '0', 'name': "sale", 'price': '33.2', 'quantity': '234',
                         'pos': 'Kiev, shukovskiy prospekt, 25', 'currency': 'UAH', 'country': 'Ukraine'}


class Payment(db.Model):
    """ """
    id = db.Column(db.Integer, primary_key=True)
    agreement = db.Column(db.String(100))
    num = db.Column(db.String(100))
    sum = db.Column(db.Float)
    payment_type = db.Column(db.String(100))
    sum_curr = db.Column(db.Float)
    currency = db.Column(db.String(100))
    date = db.Column(db.DateTime, nullable=False)
    local_dt = db.Column(db.DateTime, nullable=False)
    description = db.Column(db.String(100))
    contractor_swift = db.Column(db.String(100))

    def __init__(self, agreement, num, sum, payment_type, sum_curr, currency, date, local_dt, descr, swift):
        self.agreement = agreement
        self.num = num
        self.sum = sum
        self.payment_type = payment_type
        self.currency = currency
        self.sum_curr = sum_curr
        self.date = date
        self.local_dt = local_dt
        self.description = descr
        self.contractor_swift = swift


class PaymentSchema(ma.Schema):
    class Meta:
        fields = ['id', 'agreement', 'num', 'sum', 'payment_type', 'currency', 'sum_curr', 'date', 'local_dt',
                  'description', 'contractor_swift']


# Init payment schema
payment_schema = PaymentSchema()
payments_schema = PaymentSchema(many=True)

payment_test_case = {'id': '0', 'agreement': "Aerovei", 'num': 'OK-4532', 'sum': '234',
                         'payment_type': 'pre-sale', 'currency': 'UAH', 'sum_curr': '234.2',
                         'date': '2020-01-15', 'local_dt': '2020-01-14', 'description': 'bubble',
                         'contractor_swift': 'swift test'}


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            token = request.headers['Authorization']
            if not token:
                return do_response(0, r_code.errors['150011'], '150011')
            data = jwt.decode(token, app.config['SECRET_KEY'])
        except KeyError as err:
            print(err)
            return do_response(0, r_code.errors['150010'], '150010')
        except Exception as err:
            print(err)
            return do_response(0, r_code.errors['150012'], '150012')

        return f(*args, **kwargs)

    return decorated


@app.route('/', methods=['GET', 'POST'])
def index():
    result = {"Allowed urls":
                  {'1': '/api/errors',
                   '2': '/api/pos_on_map',
                   '3': '/api/exchange_rates',
                   '4': '/api/saldo',
                   '5': '/api/payment/<int:id_payment>',
                   '6': '/api/payments',
                   '7': '/api/pos/<int:id_pos>',
                   '8': '/api/pos',
                   '9': '/api/login',
                   '10': '/api/otp/<int:opt>',
                   '11': '/api/registration',
                   '12': '/api/invoice',
                   '13': '/api/transaction/<int:id_transaction>',
                   '14': '/api/transactions',
                   '15': '/api/user/<int:id_user>',
                   '16': '/api/users'
                   }
              }
    return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200', result)


@app.route('/api/create_trn', methods=['POST', ])
def create_trn():
    trn = Transaction("Sale", 232.2, 343, 'POS ukr test', 'UAH', 'Ukraine')
    db.session.add(trn)
    db.session.commit()

    return jsonify({'trn': "good create trn"})


@app.route('/api/create_user', methods=['POST', ])
def create_user():
    user = User('Nyx assassin', 'Nyx assassin', "Nyx assassin", '12345', 'Nyxassassin@Nyxassassin.ua', '', False)
    db.session.add(user)
    db.session.commit()

    return jsonify({'user': "good create user"})


@app.route('/api/create_payment', methods=['POST', ])
def create_payments():
    user = Payment('Local delivery', 'IK2345', 123.23, 'auto', 23.4, 'UAH', datetime.datetime.today(),
                   datetime.datetime.today(), 'for soul', 'pudge')
    db.session.add(user)
    db.session.commit()

    user = Payment('Local delivery', 'IK2345', 23.23, 'auto', 32.4, 'UAH', datetime.datetime.today(),
                   datetime.datetime.today(), 'for soul', 'voker')
    db.session.add(user)
    db.session.commit()

    user = Payment('Local delivery', 'IK2345', 432.23, 'auto', 54.4, 'UAH', datetime.datetime.today(),
                   datetime.datetime.today(), 'for soul', 'nyx')
    db.session.add(user)
    db.session.commit()

    user = Payment('Local delivery', 'OK2354', 11.23, 'auto', 12.4, 'UAH', datetime.datetime.today(),
                   datetime.datetime.today(), 'for soul', 'hren')
    db.session.add(user)
    db.session.commit()

    user = Payment('External delivery', 'IK2323', 23.23, 'auto', 233.4, 'UAH', datetime.datetime.today(),
                   datetime.datetime.today(), 'for soul', 'ban')
    db.session.add(user)
    db.session.commit()

    user = Payment('Local delivery', 'IK2345', 123.23, 'auto', 23.4, 'UAH', datetime.datetime.today(),
                   datetime.datetime.today(), 'for soul', 'pudge')
    db.session.add(user)
    db.session.commit()

    return jsonify({'user': "good create payment"})


@app.route('/api/login', methods=['POST', ])
def login():
    """ """
    try:
        username = request.json['username']
        password = request.json['password']
        otp_required = True

        if password == '12345678q' and username == "kiddo":
            token = jwt.encode({'user': username, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)},
                               app.config['SECRET_KEY'])
            if otp_required:
                data = {'jwt': token.decode('UTF-8'), 'otp': True}
                response = do_response(r_code.STATUS_SUCCESS, '', '', data)
            else:
                data = {'jwt': token.decode('UTF-8'), 'otp': False}
                response = do_response(r_code.STATUS_SUCCESS, '', '', data)
        else:
            response = do_response(r_code.STATUS_FAILURE, r_code.client_error['400'], '400')
    except KeyError as err:
        print(err)
        response = do_response(r_code.STATUS_FAILURE, r_code.client_error['400'], '400')
    except Exception as err:
        print(err)
        response = do_response(r_code.STATUS_FAILURE, r_code.server_error['500'], '500')

    return response


@app.route('/api/otp/<int:opt>', methods=['POST', ])
@token_required
def otp(otp):
    """ """
    if verify_otp(otp):
        return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200')

    return do_response(r_code.STATUS_FAILURE, r_code.client_error['400'], '400')


@app.route('/api/registration', methods=['POST', ])
def registration():
    """json_data = request.json
    user = User(
        email=json_data['email'],
        password=json_data['password']
    )
    try:
        db.session.add(user)
        db.session.commit()
        status = 'success'
    except:
        status = 'this user is already registered'
    db.session.close()"""
    return do_response(r_code.STATUS_FAILURE, r_code.server_error['501'], '501')


@app.route('/api/invoice', methods=['POST', ])
@token_required
def create_invoice():
    return do_response(r_code.STATUS_FAILURE, r_code.server_error['501'], '501')


@app.route('/api/transaction/<int:id_trn>', methods=['GET', ])
@token_required
def get_transaction(id_trn):
    try:
        if id_trn == 0:
            return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200', transaction_test_case)
    except Exception as err:
        print(err)

    return do_response(r_code.STATUS_FAILURE, r_code.server_error['600'], '600')


@app.route('/api/transactions', methods=['GET', ])
@token_required
def get_transactions():
    return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200', transaction_test_case)


@app.route('/api/user/<int:id_user>', methods=['GET', ])
@token_required
def get_user(id_user):
    try:
        if id_user == 0:
            return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200', user_test_case)
    except Exception as err:
        print(err)

    return do_response(r_code.STATUS_FAILURE, r_code.client_error['404'], '404')


@app.route('/api/users', methods=['GET', ])
@token_required
def get_users():
    try:
        return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200', user_test_case)
    except Exception as err:
        print(err)
    return do_response(r_code.STATUS_FAILURE, r_code.client_error['404'], '404')


@app.route('/api/pos', methods=['GET', ])
def get_poses():
    return do_response(r_code.STATUS_FAILURE, r_code.server_error['501'], '501')


@app.route('/api/pos/<int:id_pos>', methods=['GET', ])
def get_pos(id_pos):
    return do_response(r_code.STATUS_FAILURE, r_code.server_error['501'], '501')


@app.route('/api/payments', methods=['GET', ])
@token_required
def get_payments():
    return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200', payment_test_case)


@app.route('/api/payment/<int:id_pmnt>', methods=['GET', ])
@token_required
def get_payment(id_pmnt):
    try:
        if id_pmnt == 0:
            return do_response(r_code.STATUS_SUCCESS, r_code.success['200'], '200', payment_test_case)
    except Exception as err:
        print(err)

    return do_response(r_code.STATUS_FAILURE, r_code.client_error['404'], '404')


@app.route('/api/saldo', methods=['GET', ])
def get_saldo():
    return do_response(r_code.STATUS_FAILURE, r_code.server_error['501'], '501')


@app.route('/api/exchange_rates', methods=['GET', ])
def get_exchange_rates():
    return do_response(r_code.STATUS_FAILURE, r_code.server_error['501'], '501')


@app.route('/api/pos_on_map', methods=['GET', ])
def get_pos_on_map():
    return do_response(r_code.STATUS_FAILURE, r_code.server_error['501'], '501')


@app.route('/api/errors', methods=['GET', ])
@token_required
def get_errors():
    results = r_code.success
    results.update(r_code.redirection)
    results.update(r_code.information)
    results.update(r_code.server_error)
    results.update(r_code.client_error)

    return jsonify({'errors': results})


# Run Server
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=13131)
