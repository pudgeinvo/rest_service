import flask
import json

from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()


def to_json(data):
    """ """
    return json.dumps(data) + "\n"


def response(code, data):
    """ """
    return flask.Response(
        status=code,
        mimetype="application/json",
        response=to_json(data)
    )


@auth.verify_password
def verify_password(username_or_token, password):
    pass
    """user = models.User.verify_auth_token(username_or_token)
    if not user:
        user = models.User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True"""


def verify_otp(otp):
    """ """
    # connect to LinOTP
    # check opt
    # if correct, return True
    # if incorrect, return False
    return True


def do_response(status, message='', error='', data=''):
    """ """
    result = {'status': status}

    if message:
        result['message'] = message
    if error:
        result['error'] = error
    if data:
        result['data'] = data

    return flask.jsonify(result)
