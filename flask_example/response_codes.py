#: utf-8

STATUS_SUCCESS = 1
STATUS_FAILURE = 0

success = {
    '200': 'OK',
    '201': 'Created',
    '202': 'Accepted',
    '203': 'Non-authoritative information',
    '204': 'No content',
    '205': 'Reset content',
    '206': 'Partial content'
}

redirection = {
    '300': 'Multiple choices',
    '301': 'Moved permanently',
    '302': 'Found',
    '303': 'See other',
    '304': 'Not modified',
    '305': 'Use proxy',
    '307': 'Temporary redirect',
    '308': 'Permanent redirect'
}

information = {
    '100': 'continue',
    '101': 'switching protocols',
    '102': 'processing'
}

server_error = {
    '500': 'Internal Server Error',
    '501': 'Not Implemented',
    '502': 'Bad Gateway',
    '503': 'Service Unavailable',
    '504': 'Gateway Timeout',
    '505': 'HTTP Version Not Supported',
    '506': 'Variant Also Negotiates',
    '507': 'Insufficient Storage',
    '508': 'Loop Detected',
    '510': 'Not Extended',
    '511': 'Network Authentication Required',
    '599': 'Network Connect Timeout Error',
    '600': 'Transaction not found!',
    '601': 'User not found!',
}

client_error = {
    '400': 'Bad Request',
    '401': 'Unauthorized',
    '402': 'Payment Required',
    '403': 'Forbidden',
    '404': 'Not Found',
    '405': 'Method Not Allowed',
    '406': 'Not Acceptable',
    '407': 'Proxy Authentication Required',
    '408': 'Request Timeout',
    '409': 'Conflict',
    '410': 'Gone',
    '411': 'Length Required',
    '412': 'Precondition Failed',
    '413': 'Payload Too Large',
    '414': 'Request-URI Too Long',
    '415': 'Unsupported Media Type',
    '416': 'Requested Range Not Satisfiable',
    '417': 'Expectation Failed',
    '418': "I'm a teapot",
    '421': 'Misdirected Request',
    '422': 'Unprocessable Entity',
    '423': 'Locked',
    '424': 'Failed Dependency',
    '426': 'Upgrade Required',
    '428': 'Precondition Required',
    '429': 'Too Many Requests',
    '431': 'Request Header Fields Too Large',
    '444': 'Connection Closed Without Response',
    '451': 'Unavailable For Legal Reasons',
    '499': 'Client Closed Request'
}

errors = {
    '150001': 'Credentials is incorrect!',
    '150002': 'OTP is incorrect!',
    '150003': 'Unexpected error!',
    '150004': 'User not found!',
    '150005': 'User or password is wrong!',
    '150006': 'Bad request!',
    '150007': 'Successful!',
    '150008': 'Created!',
    '150009': 'User or password is wrong!',
    '150010': 'Authorization header not found!',
    '150011': 'Token is missing!',
    '150012': 'Token is invalid!',
}

